import json

from django import http
from django.conf import settings
from django.http import HttpResponseRedirect

# импорты из собственного пакета (относительные ссылки)
from .models import User
from .const import REG_PACKAGE_URL


def check_first_run_decorator(f):
    """ Декоратор проверяет наличие хотя бы одного администратора системы.
        В случае если его нет, перенаправляет на страницу создания администратора.
    """
    def wrapper(*args, **kwargs):
        if not User.objects.filter(is_superuser=True).exists():
            # в таблице User нет ни одного администратора системы
            return http.HttpResponseRedirect('%s/create-admin' % REG_PACKAGE_URL)
        return f(*args, **kwargs)

    return wrapper


def authenticated_user_required(f):
    """ Декоратор что пользователь аутентифицирован
    """
    def wrapper(request, *args, **kwargs):
        user = request.user
        if not settings.DISABLE_AUTH and (not user or not user.is_authenticated()):
            if request.is_ajax():
                result = json.JSONEncoder().encode(
                    {'success': False,
                     'message': 'Вы не авторизованы. Возможно, закончилось'
                                ' время пользовательской сессии. Для'
                                ' повторной аутентификации обновите'
                                ' страницу.'})
                return http.HttpResponse(result, mimetype='application/json')
            else:
                return HttpResponseRedirect('%s/login/' % REG_PACKAGE_URL)
        else:
            return f(request, *args, **kwargs)

    return wrapper
