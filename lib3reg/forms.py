from django import forms

from django.contrib.auth.forms import AuthenticationForm
from django.forms.utils import ErrorList

from .models import User, RegistrationProfile

attrs_dict = {'class': 'required'}
USERNAME_USED_MESSAGE = 'Этот логин уже используется. Пожалуйста, введите другой.'
EMAIL_USED_MESSAGE = 'Этот email уже используется. Пожалуйста введите другой.'


class RegAuthenticationForm(AuthenticationForm):
    """ class for authenticating users. Extend this to get a form that accepts
        username/password logins.
    """
    username = forms.CharField(
        label="Имя пользователя",
        max_length=254
    )
    password = forms.CharField(
        label="Пароль",
        widget=forms.PasswordInput
    )

    error_messages = {
        'invalid_login': "Пожалуйста, введите правильное имя пользователя и пароль. "
                         "Обратите внимание, что оба поля могут быть чувствительны к регистру.",
        'inactive': "Этот аккаунт не активен.",
    }


class RegistrationForm(forms.Form):
    """
        Форма для регистрации новой учетной записи пользователя.         

        Проверяет, что запрашиваемая имя пользователя не используется,
        и требуется пароль, который необходимо ввести в два раза.         
    """
    username = forms.RegexField(
        regex=r'^\w+$',
        max_length=30,
        widget=forms.TextInput(attrs=attrs_dict),
        label='Логин'
    )
    email = forms.EmailField(
        widget=forms.EmailInput(attrs=attrs_dict),
        label='Email'
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
        label='Пароль'
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
        label='Пароль (повтор)'
    )

    # noinspection PyUnusedLocal
    def __init__(
            self, request, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
            label_suffix=None, empty_permitted=False, field_order=None):
        super(RegistrationForm, self).__init__(
            data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, field_order)

    def clean_username(self):
        """ Проверяет что имя пользователя состоит из букв и не используется
        """
        try:
            User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(USERNAME_USED_MESSAGE)

    def clean_password2(self):
        """ Проверяет, что значения, введены в оба поля и введенные в обоих полях пароля значения идентичны
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError('Вы должны ввести одинаковый пароль в двух полях.')
        return self.cleaned_data['password2']

    def clean_email(self):
        """ Проверяет что email уникальный
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(EMAIL_USED_MESSAGE)

        return self.cleaned_data['email']

    def save(self, profile_callback=None):
        """ Создает нового пользователя и возвращает объект User
        """
        new_user = RegistrationProfile.objects.create_inactive_user(
            username=self.cleaned_data['username'],
            password=self.cleaned_data['password1'],
            email=self.cleaned_data['email'],
            profile_callback=profile_callback
        )
        return new_user
