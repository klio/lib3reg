import os

from django.conf import settings
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from .forms import RegistrationForm, RegAuthenticationForm
from .const import ACTIVATION_URL
from .views import activate, register

_PATH = os.path.realpath(os.path.dirname(__file__))

urlpatterns = [

    url(r'^%s/(?P<activation_key>\w+)/$' % ACTIVATION_URL,
        activate,
        name='registration_activate'),

    url(r'^$',
        auth_views.login,
        {'template_name': os.path.join(_PATH, 'templates/login.html')}),

    url(r'^login/$',
        auth_views.login,
        {
            'template_name': os.path.join(_PATH, 'templates/login.html'),
            'authentication_form': RegAuthenticationForm
         },
        name='auth_login'),

    url(r'^logout/$',
        auth_views.logout,
        {
            'template_name': os.path.join(_PATH, 'templates/logout.html'),
            'extra_context': {'button_title': 'OK', 'button_link': settings.LOGIN_REDIRECT_URL},
         },
        name='auth_logout'),

    url(r'^password/change/$',
        auth_views.password_change,
        {'template_name': os.path.join(
            _PATH, 'templates/password_change_form.html')},
        name='auth_password_change'),

    url(r'^password/change/done/$',
        auth_views.password_change_done,
        {'template_name': os.path.join(
            _PATH, 'templates/password_change_done.html')},
        name='auth_password_change_done'),

    url(r'^password/reset/$',
        auth_views.password_reset,
        {'template_name': os.path.join(
            _PATH, 'templates/password_reset_form.html'),
         'email_template_name': os.path.join(
             _PATH, 'templates/password_reset_email.html'),
         'post_reset_redirect': auth_views.password_reset_done},
        name='auth_password_reset'),

    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        auth_views.password_reset_confirm,
        {'template_name': os.path.join(
            _PATH, 'templates/password_reset_confirm.html')},
        name='auth_password_reset_confirm'),

    url(r'^password/reset/complete/$',
        auth_views.password_reset_complete,
        {'template_name': os.path.join(
            _PATH, 'templates/password_reset_complete.html')},
        name='auth_password_reset_complete'),

    url(r'^password/reset/done/$',
        auth_views.password_reset_done,
        {'template_name': os.path.join(
            _PATH, 'templates/password_reset_done.html')},
        name='auth_password_reset_done'),

    url(r'^reguser/$',
        register,
        {
            'template_name': os.path.join(_PATH, 'templates/registration_form.html'),
            'form_class': RegistrationForm
        },
        name='registration_register'),

    url(r'^reguser/complete/$',
        TemplateView.as_view(template_name=os.path.join(_PATH, 'templates/registration_complete.html')),
        name='registration_complete'),
]
