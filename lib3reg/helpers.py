from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail

from .const import REG_PACKAGE_URL, ACTIVATION_URL


def send_activation_key(user, registration_profile):

    subject = render_to_string(
        'activation_email_subject.txt',
        {'site': settings.CURRENT_SITE_HOST})
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())

    message = render_to_string(
        'activation_email.txt',
        {'activation_key': registration_profile.activation_key,
         'expiration_date': registration_profile.expiration_date_str(),
         'username': user.username,
         'site': settings.CURRENT_SITE_HOST,
         'activation_url': '%s/%s' % (REG_PACKAGE_URL, ACTIVATION_URL)})

    send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, [user.email])
